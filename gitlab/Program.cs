﻿using System;

namespace gitlab
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            string input;
            int num;
            int sum = 0;
            Console.Write("Enter a number:");
            input = Console.ReadLine();
            num = Convert.ToInt32(input);
            for (int i = 0; i <= num; i++)
            {
                Console.WriteLine(i);
                sum += i;
            }
            Console.WriteLine("The sum is: "+sum);

        }
    }
}
